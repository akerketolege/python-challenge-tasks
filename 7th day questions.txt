Dictionary is a collection of keys and values. They are usually indexed, unordered, could be changed. Duplicates are not allowed They can be made using a funciton dict()
or using curly brackets. e.g.:
dictionary = {
"student":"Akerke",
"class":"12",
"challenge":"Python"
}
IN this case, student, class, and challenge are the keys, and Akerke, 12, Python are the values.
Functions are similar in most programing languages, and in Python they are made to make some particular things when they are called out.
In python it should be defined using "def" and have to have "()" at the end. In the brackets variable names could be included. For example:
def printing():
    print("This is the point")
#Calling the function to execute it. 
printing()
Lists are ordered, changeable. It is also a collection of values, however, the duplicate members are allowed, and they are made using square brackets.
e.g.: 
newlist = [5,8,9]